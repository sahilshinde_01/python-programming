
class Outer:
    
    def __init__(self):
        print("Outer Constructor")
        self.x = 10
        self.Inner = self.Inner()

    def dispData(self):
        print(self.x)
        self.Inner.dispData()

    class Inner:

        def __init__(self):
            print("Inner constructor")
            self.y = 20

        def dispData(self):
            print(self.y)

outObj = Outer()
outObj.dispData()
