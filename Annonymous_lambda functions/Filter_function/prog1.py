#print even num from list using filter func

"""
def even(x):
    if x%2==0:
        return True
    else:
        return False

lst = [10,11,12,13,14]

for x in filter(even,lst):
    print(x,)

OR
"""

def even(x):
    if x%2==0:
        return True
    else:
        return False

lst = [10,11,12,13,14]

list1 =list(filter(even,lst))
print(list1)

