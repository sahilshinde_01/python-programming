
#       Global variable         (only accessed in its scope)

x = 10

def fun1():
    global x
    print(x)                        #10
    x = 20
    print(x)                        #20

fun1()   
print(x)                            #10


