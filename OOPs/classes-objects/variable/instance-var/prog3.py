
class student:
    
    def __init__(self,name,ID):
        print("In constructor")
        self.name = name
        self.ID = ID
    
    def dispData(self):
        print(self.name)
        print(self.ID)

print("Enter stud1 Data : ")

name = input("Enter name : ")
ID = int(input("Enter ID : "))

obj1 = student(name,ID)
obj1.dispData()

print("Enter stud2 Data : ")

name = input("Enter name : ")
ID = int(input("Enter ID : "))

obj2 = student(name,ID)
obj2.dispData()

