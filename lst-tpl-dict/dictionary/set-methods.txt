

	Set methods -

- add
- clear
- copy
- difference
- difference update
- discard
- intersection
- intersection update
- isdisjoint
- issubset
- issuperset
- pop
- remove
- symetric_difference_update
- union
- update

