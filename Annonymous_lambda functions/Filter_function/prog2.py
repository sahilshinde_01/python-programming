#filter capital letter
"""
def cap(x):

    if ('A'<=x and 'Z'>=x):
        return True
    else:
        return False

for x in filter(cap,input("Enter a string : ")):
    print(x , end = "   ")

                    OR
"""
#using lamda function

lst = [1,2,3,4,5]
list1 = list(filter(lambda x : (x%2==0),lst))
print(list1)

