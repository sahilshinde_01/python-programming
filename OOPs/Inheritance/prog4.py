
class Gold:
    atomNo = 69

    @classmethod
    def ChemicalProp(self):
        print("Noble  Metal")

    def __init__(self):
        self.price = 52200
    
    def CurrentPrice(self):
        print(self.price)
    
class GoldOrnaments(Gold):
    
    def __init__(self):
        super().__init__()
        self.weight = int(input("Enter weight of gold : "))

    def bill(self):
        print("Total amount = ",(self.weight*self.price) )

obj = GoldOrnaments()
obj.CurrentPrice()
obj.bill()
