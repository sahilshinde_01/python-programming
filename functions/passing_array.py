

#       Passing array to a function

import numpy

def arrSum(arrObj):
    sum = 0

    for x in arrObj:
        sum = sum + x
        
    print(sum)

arrSize = int(input("Enter size of array : "))

arrObj = numpy.zeros(arrSize,int)

for x in range(arrSize):
    arrObj[x] = int(input("Enter element : "))

print(arrObj)

arrSum(arrObj)

