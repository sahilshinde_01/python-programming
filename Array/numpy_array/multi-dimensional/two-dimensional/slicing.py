

#                Syntax -  arrayname [[row] [col]]                      (row compulsory needed)

import numpy

arr = numpy.array([[1,2,3],[4,5,6],[7,8,9]])

print(arr[0:0])

#row

print(arr[0:2:1])
print(arr[1:2:1])
print(arr[2: : ])
print(arr[-1:-3: ])
print(arr[-3:-1: ])

#row and col

print(arr[ : 1 : ])
print(arr[2:1: ])
print(arr[ :-3:1])

#print(arr[ :-3:-1:-1])                      #error

