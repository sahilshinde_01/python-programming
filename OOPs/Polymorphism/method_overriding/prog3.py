

class Parent:
    @classmethod 
    def show(cls):
        print(cls.x)
        
class Child(Parent):
    x = 20
    def __init__(self):
        self.x = 20

    def show(self):
        print(self.x)
        super().show()

obj = Child()
obj.show()

