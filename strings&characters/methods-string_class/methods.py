
str1 = "core2web" 
str2 = "Core2web"


# capitalise

print(str1.capitalize())                                #Core2web

# casefold

print(str1.casefold())                                  #true

# center

print(str1.center(20,'x'))                              #xxxxxCore2webxxxxx

# count

print(str1.count('e'))                                  #2

# endswith

print(str1.endswith("web"))                            #true

# fin()

print(str1.find("web"))                                #5
print(str1.rfind("web"))                               #13

# index

print(str1.index("web"))
print(str1.rindex("web"))

# join

print(str1.join(""))
#print(xyz.join(str1))

# ljust

print(str1.ljust(10,"x"))
print(str1.rjust(10,"x"))

# lower 

print(str1.lower())

#strip()                                            #deletes spaces

print(str1.strip())
print(str1.lstrip())
print(str1.rstrip())

# partition                                         #2 parts

print(str1.partition("  "))                           #(start,separator,end)
print(str1.rpartition(","))

# replace

print(str1.replace("web","mobile"))

#immutable string

str3 = "Core2web"

#str3[3] = "x"                                   #error



str1 = "core2web Techonologies Biencaps"

# split

print(str1.split(" "))

# startswith

print(str1.startswith("core"))                  #true that means string str1 start with 'core' or not

# swapcase

print(str1.swapcase())

# title
str4 = "Python"
print(str4.title())

# upper

print(str1.upper())                             # prints all in caps

# zfill

print(str1.zfill(10))                           # 0000core2web ....

# string testing methods

str1 = "Flutter 2022"
print(str1.isalnum())                           # is all characters number

# isalpha

print(str1.isalpha())                           # false


# is ascii

print(str1.isascii())

# isdecimal

print(str1.isdecimal())

# isdigit

print(str1.isdigit())

# isnumeric

print(str1.isnumeric())

# isidentifier

print(str1.isidentifier())

# iskeyword
str2 = "def"
print(str1.iskeyword(str2))          













