
class Cric:
    
    format = "T20"

    def __init__(self,name,jersey):                 # constructor / instance variable
        self.name = name
        self.jersey = jersey
    
    def dispData(cls):                              # instance method
        print(cls.name)
        print(cls.jersey)

player1 = Cric("Dhoni",7)
player2 = Cric("Virat",18)

player1.dispData()
player2.dispData()

