

class Outer:

    x = 10

    def __init__(self):
        print("Outer Constructor")
        self.out = 10

    class Inner:
        
        y = 20

        def __init__(self):
            print("Inner constructor")
            self.inner = 20

        def dispInner(self):
            print(self.inner)
            print(self.y)

    def dispOuter(self):
        print(self.out)
        print(self.x)
    
outObj = Outer()
outObj.dispOuter()

innerObj = outObj.Inner()
innerObj.dispInner()


    
