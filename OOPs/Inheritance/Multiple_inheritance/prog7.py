

class A:
    
    def __init__(self):
        print("A Constructor")

class B(A):

    def __init__(self):
        print("B Constructor")
        self.y = 20

class C(A):

    def __init__(self):
        print("C Constructor")
        self.z = 30

class D(B,C):
    
    def __init__(self):
        print("D Constructor")
       #super().__init__()

    def valueX(self):
        print(self.x)
        print(self.y)
        print(self.z)

obj = D()
obj.valueX()

