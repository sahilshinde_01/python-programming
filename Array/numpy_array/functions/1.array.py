
import numpy

arr = numpy.array([10,20,30,40,50])

print(arr)
print(type(arr))
print(type(arr[0]))

arr = numpy.array([10,20,30.5,40,50])

print(arr)
print(type(arr))
print(type(arr[2]))

arr1 = numpy.array([10,20,30,40,50],int)

print(arr1)
print(type(arr1))
print(type(arr1[0]))

arr2 = numpy.array([10,20,30,40,50],float)

print(arr2)
print(type(arr2))
print(type(arr2[0]))
