from abc import ABC,abstractmethod

class Parent:

    @abstractmethod
    def show():
        None
    
    @abstractmethod
    def info():
        None


class Child(Parent):

    def show(self):
        print("In Info")

class Xyz:
    pass

obj = Child()
obj.show()

print(type(Parent))
print(type(Child))
print(type(Xyz))


