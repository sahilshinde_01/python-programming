 
class Parent(object):
    x = 10 
    def __init__(self):
        print("Parent Constructor")
        self.y = 20

    @classmethod
    def show(cls):
        print(cls.x)

    def disp(self):
        print(self.x)
        print(self.y)

class Child(Parent):
    x = 110

    def __init__(self):
        print("Child Constructor")
        #super.__init__()

    def childDisp(self):
        print(super().x)
        print(self.x)

obj = Child()
obj.childDisp()

