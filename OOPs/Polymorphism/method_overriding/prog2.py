

class Parent:
    x = 10
    def view(self):
        print(self.x)
        
class Child(Parent):
    x = 20
    def show(self):
        print(self.x)

obj = Child()
obj.show()
obj.view()
