
# wrong approach

'''
def employeeInfo(name,sal):
	print("Employee name =",name)
	print("Employee sal =",sal)

employeeInfo("Kanha",65.5)
'''

# right approach

def employeeInfo(name,sal):
	print("Employee name =",name)
	print("Employee sal =",sal)

employeeInfo(name ="Kanha",sal = 65.5)

