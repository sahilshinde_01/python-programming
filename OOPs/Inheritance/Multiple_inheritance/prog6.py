

class X:
    
    def __init__(self):
        super().__init__()
        print("X Constructor")

class A(X):

    def __init__(self):
        super().__init__()
        print("A Constructor")

class B(X):

    def __init__(self):
        super().__init__()
        print("B Constructor")

class D:
    
    def __init__(self):
        super().__init__()
        print("D Constructor")

class C:

    def __init__(self):
        super().__init__()
        print("C Constructor")

obj = C()


