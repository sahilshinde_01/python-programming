
company = list()


# append 

company.append("Meta")
company.append("Apple")
company.append("Amazon")
company.append("Netflix")
company.append("Google")
print("Append : ",company)

# clear

# company.clear()

print("Clear : ",company)

# copy

lst2 = company.copy()
print("Copy : ",lst2)

# count

print("Count : ",company.count("Google"))

# extend

company.extend(["Amdocs","Pubmatic"])                        # 'A' 'M' 'D' 'O' 'C' 'S'
print("Extend : ",company)

# index

#print(company.index(5))                         #value error : 5 not in list
print("Index : ",company.index("Pubmatic",2,7))                         #value error : 5 not in list
print("Index : ",company.index("Pubmatic"))

# insert

company.insert(5,"Uber")
print("Insert : ",company)

# pop

company.pop()
print("Pop : ",company)

# remove

company.remove("Uber")
print("Remove : ",company)          

# reverse

company.reverse()
print("Reverse : ",company)                             #reverses 

# sort

company.sort()                                          # sorts alphabetically
print("Sort : ",company)                                
