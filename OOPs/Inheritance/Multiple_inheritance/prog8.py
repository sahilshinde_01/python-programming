
class A:
    
    def __init__(self):
        print("A Constructor")
        self.x = 400

    def valueX(self):
        print(self.x)

class B(A):
    
    def __init__(self):
        print("B Constructor")
        self.x = 300
        super().__init__()      

    def valueX(self):
        print(self.x)
        super().valueX()

class C(A):

    def __init__(self):
        print("C Constructor")
        self.x = 200
        super().__init__()
    
    def valueX(self):
        print(self.x)
        super().valueX()

class D(B,C):
    
    def __init__(self):
        print("D Constructor")
        self.x = 100
        super().__init__()

    def valueX(self):
        print(self.x)
        super().valueX()

obj = D()
obj.valueX()


