                                            #Writing our own meta class



from abc import ABCMeta,ABC

class CWMeta(type):
    def __new__(cls,clsname,basecls,clsdict):
        print(cls)
        print(clsname)
        print(basecls)
        print(clsdict)


        if len(clsdict)==2:
            raise TypeError("2 arguments not allowed")

        return super().__new__(cls,clsname,basecls,clsdict)

class Child(CWMeta):

    pass

obj2 = CWMeta('CWMeta',(),{})
obj2 = Child('Child',(),{'a':20,'b':30,'d':50})
obj2 = Child('Child',(),{'a':20})
obj2 = Child('Child',(),{'a':20,'b':30})
