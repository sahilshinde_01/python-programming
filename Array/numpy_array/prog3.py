
import array


arr1 = array.array('i',[10,20,30,40,50])

arr2 = array.array('i',[1,2,3,4,5])

arr3 = arr1 - arr2                  #Error 
    
arr4 = arr2 - arr1                  #Error

print(arr1)
print(arr2)
print(arr3)
print(arr4)
