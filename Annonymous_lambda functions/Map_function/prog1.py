#use map_function for multiplication

def mult(x,y):
    return x*y

list1 = [1,2,3,4,5]
list2 = [6,7,8,9,10]

list3 = list(map(mult,list1,list2))
print(list3)
