
class Cric:

    matchFormat = "T20"

    # decorator

    def myclassmethod(fun):

        def inner(*args):
            fun(args[0].__class__)

        return inner

    def __init__(self):
        self.name = "Dhoni"
        self.jerNo = 7


    # instance variable

    def disp(self):
        print("Name = {} and JerNo = {} ".format(self.name,self.jerNo))


    # class method 
    
    @myclassmethod
    def dispFormat(cls):
        print(cls)
        print(cls.matchFormat)

player1 = Cric()
player1.disp()

print(player1)

player1.dispFormat()



