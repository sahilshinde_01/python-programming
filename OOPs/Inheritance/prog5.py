
class Company:
    #class variable
    cname = "Veritas"
    workspace = "Baner"


    #constructor 
    def __init__(self):
        self.teamCode = "Python Code"
        print("Company Constructor")
    
    @classmethod
    def facilities(cls):
        print("All facilities")
        print(cls.cname)
        print(cls.workspace)

class Employee(Company):

    #class variable
    role = "Developer"
    
    #constructor
    def __init__(self,empId,lang):
        self.empId = empId
        self.lang = lang
        print("Child Constructor")
        super().__init__()

    def info(self):
        print(self.empId)
        print(self.lang)
        print(self.role)
        print(self.workspace)

    @classmethod
    def skillSet(cls):
        print(cls.role)


emp1 = Employee(25,"Python")
emp2 = Employee(35,"Java")
emp1.info()
emp2.info()














