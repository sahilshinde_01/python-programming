
import numpy

arr1  = numpy.matrix([[1,2,3],[4,5,6],[7,8,9]])

print(numpy.sort(arr1,axis = 0))                        #by default 1 ,0 - col wise sort, 1 - rows wise sort
                
arr1 = numpy.matrix([[21,3,15],[9,15,13],[7,4,14]])
print(arr1)

print(numpy.sort(arr1))
