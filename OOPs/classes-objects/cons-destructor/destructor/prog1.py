
class Movies:

    def __init__(self):
        print("In constructor")
        self.mname = "RRR"
        self.director = "SSS"

    def info(self):
        print(self.mname)
        print(self.director)
     
    def __del__(self):
        print("Deleting Object")

Movie1 = Movies()
print(Movie1)
Movie1.info()

movie1.__del__()

movie2 = Movies()

print(movie2)
movie2.info()

print(movie1)


