
#                        view (shallow copy)     and     copy (deep copy)


import numpy

arr1 = numpy.array([10,20,30,40,50])

arr2 = arr1.copy()                          #view()

print(id(arr1))
print(id(arr2))

print(arr1) 
print(arr2)

arr2[2] = 60

print(arr1)
print(arr2)
