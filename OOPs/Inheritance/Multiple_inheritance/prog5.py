

class A:
    
    def __init__(self):
        super().__init__()
        print("A Constructor ")

class B:

    def __init__(self):
        print("B Constructor")

class D:

    def __init__(self):
        print("D Constructor")

class C(A,B,D):

    def __init__(self):
        super().__init__()
        print("C Constructor")

obj = C()
