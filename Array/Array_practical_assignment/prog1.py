'''

wap to find out even number from the array

'''

import array
iArr = array.array('i',[])
num = int(input("Enter count of elements : "))

for i in range(num):
    ele = int(input("Enter a num : "))
    iArr.append(ele)

for ele in iArr:    
    if ele%2 == 0:
        print(ele , end =" ")
print()
