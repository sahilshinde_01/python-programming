
# wrong

"""
def fact(num):

    sum = 1

    if(num>0):
        sum = sum *num
        fact(num-1)

    return sum

fact = fact(int(input("Enter a num : ")))

print("Factorial is : ",fact)
"""

# correct

def fun(num):

    return num*fun(num-1)

print(fun(5))
