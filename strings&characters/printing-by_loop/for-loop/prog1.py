

str1 = input("Enter string : ")


# normal string (not reverse)

for x in str1:                          
    print(x , end ='')                  #python
print()

for x in range(len(str1)):              #0 1 2 3 4 5
    print(str1[x] , end ="")                  
print()


# reverse string

for x in range(1,len(str1)+1):
    print(str1[-x],end ='')
print()



