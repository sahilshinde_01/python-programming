

from abc import ABC,abstractmethod



class OS(ABC):

    def bbInfileSys(self):
        print("Boot block concept same for all OS")

    def HALinterface(self):
        print("Every OS has HAL interface")

    @abstractmethod
    def fileSystem(self):
        pass

class Windows(OS):

    def fileSystem(self):
        print("Resilient File System")


user = Windows()
user.HALinterface()
user.bbInfileSys()
user.fileSystem()

