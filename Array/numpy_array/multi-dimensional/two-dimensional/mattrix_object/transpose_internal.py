
import numpy

arr1  = numpy.matrix([[1,2,3],[4,5,6],[7,8,9]])

arr2 = numpy.zeros(3,3)

for x in range(3):
    for y in range(3):
        arr2[x][y] = arr1[x][y]

print(arr2)
