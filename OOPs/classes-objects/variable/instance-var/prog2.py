
class Employee:

    cname = "Veritas"

    def __init__(self):
        print("In constructor")
        self.ename = "Jeevan"
        self.empId = "270"

    def disp(self):
        print(self.ename)
        print(self.empId)
        print('empName = {} and empId = {}'.format(self.ename,self.empId))

obj1 = Employee()
obj1.disp()

