rows = 12
num = 65
ch = 1

for x in range(rows):
    for y in range(3):
        if(x%3==0):
            print("*",end="   ")
        elif(x%3==1):
            print(chr(num),end="   ")
            num=num+1
        elif(x%3==2):
            print(ch,end="   ")
            ch= ch+1
    print()
