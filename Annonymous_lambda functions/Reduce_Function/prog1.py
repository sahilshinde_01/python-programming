#use reduce function for add and factorial

#addition

import functools

def sum(x,y):
    return x+y

x = functools.reduce(sum , [1,2,3,4,5])
print("The addition is : ",x)

#factorial

import functools

def fact(x,y):
    return x*y

x = functools.reduce(fact, [1,2,3,4,5])
print("The factorial is : ",x)

