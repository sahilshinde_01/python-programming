
import array

iArr = array.array('I',[10,20,30,40])
print(iArr)

"""iArr[4] = 50     #error : array assignment index out of range
print(iArr)"""

#1.append

iArr.append(50)
iArr.append(60)
iArr.append(70)
iArr.append(80)
iArr.append(90)
iArr.append(30)
print("Append :",iArr)

#2.count

print("Count 30 :",iArr.count(30))           #2
print("Count 40:",iArr.count(40))           #1

#3.index

print("Index 60: ",iArr.index(60))           #5

#4.insert

iArr.insert(3,35)
print("Insert 35:",iArr)

#5.pop

iArr.pop()
print("Pop :",iArr)                         #default last

#6.remove

iArr.remove(50)
print("Remove 50:",iArr)

#7.reverse

iArr.reverse()
print("Reverse :",iArr)

#8.fromlist

lst = [1,2,3,4]
print(iArr)
iArr.fromlist(lst)
print("From list",iArr)

#10.buffer_info                             #counts  elements in array

print("Buffer_info :",iArr.buffer_info())           
