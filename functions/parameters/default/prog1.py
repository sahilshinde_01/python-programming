
def fun(x , y = 20):
    print(x,y)
    #print(y)

fun(10,30)                          #10 30
fun(30)                             #30 30
fun()                               #error: missing one argument

