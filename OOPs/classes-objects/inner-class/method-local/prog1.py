
class Outer:
    
    def __init__(self):
        print("Outer Constructor")
        self.x = 10

    def dispData(self):
        print(self.x)

    class Inner:

        def __init__(self):
            print("Inner Constructor")
            self.y = 20

        def dispData(self):
            print(self.x)

    z = 30
    print(z)
    
    innObj = Inner()
    innObj.dispData()

    print(innObj)

outObj = Outer()
innObj.dispData()
