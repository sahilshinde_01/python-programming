'''
wap to find odd numbers from the array


'''
import array

iArr = array.array('i',[])

num = int(input("Enter a count of element : "))
    
for i in range(num):
    ele = int(input("Enter num : "))
    iArr.append(ele)

for ele in iArr:
    if ele%2!=0:
        print(ele, end = " ")
print()
