
class Parent(object):
    
    x = 10
    def __init__(self):
        self.y = 20
        print("Parent Constructor")

    @classmethod
    def show(self):
        print(self.x)
        
    def disp(self):
        print(self.x)
        print(self.y)

class Child(Parent):

    def __init__(self):
        print("Child Constructor")
       #super().__init__()

    def ChildDisp(self):
        print(self.x)
       #print(self.y)

obj = Child()
obj.ChildDisp()
obj.show()
obj.disp()






