
def fun(x = 10 , y):                    #error: non default argument follows default argument 
    print(x,y)
    #print(y)

fun(10,30)                          
fun(30)                             
fun()                               

