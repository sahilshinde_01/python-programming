
class Movies:

    def __init__(self):
        print("In constructor")
        self.mname = "RRR"
        self.director = "SSS"

    def info(self):
        print(self.mname)
        print(self.director)
     
    def __new__(self):
        print("Memory allocation...")
        return super().__new__(self)                # calls first
      

Movie1 = Movies()
print(Movie1)
Movie1.info()


