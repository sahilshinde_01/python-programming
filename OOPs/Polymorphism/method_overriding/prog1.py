

class Parent:
    x = 20
    def show(self):
        print(self.x)
        print(super().x)

obj = Child()
obj.show()
