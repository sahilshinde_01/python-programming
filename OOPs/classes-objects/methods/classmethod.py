
class Cric:
    
    format = "T20"

    def __init__(self,name,jersey):                 # constructor / instance variable
        self.name = name
        self.jersey = jersey
    
    @classmethod
    def dispData(cls):                              # class method
        print(cls)
        print(cls.format)

player1 = Cric("Dhoni",7)
player2 = Cric("Virat",18)

Cric.dispData()
player1.dispData()

