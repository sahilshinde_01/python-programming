
class Company:
    
    def __init__(self):
        print("In Company Constructor")
        print()
        self.cname = input("Enter Company name : ")
        self.noofemp = int(input("Enter no. of emp : "))

    def CompanyInfo(self):
        print(self.cname)
        print(self.noofemp)

class Employee(Company):
    
    def __init__(self):
        super().__init__()
        print("In Employee Constructor")
        print()
        self.Ename = input("Enter emp name : ")
        self.Eid   = int(input("Enter empId : "))

    def EmpInfo(self):
        print(self.Ename)
        print(self.Eid)

obj = Employee()
obj.CompanyInfo()
obj.EmpInfo()

